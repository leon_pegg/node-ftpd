/*jslint node: true, nomen: true*/
'use strict';

var net = require('net'),
    util = require('util'),
    path = require('path'), // Unsure if this is needed here
    colors = require('colors'),
    logger = require('./logger'),
    messages = require('./messages'),
    commands = require('./commands');

function createServer(options) {
    var server = net.createServer();

    server.on('listening', function () {
        logger.log('info', 'Listening on ' + server.address().address + ':' + server.address().port);
    });

    server.on('connection', function (socket) {
        server.emit('client:connected', socket);

        socket.setTimeout(0);
        socket.setEncoding('ascii');
        socket.setNoDelay();

        socket.passive = false;
        socket.dataInfo = null;
        socket.dataSocket = null;
        socket.mode = 'ascii';

        socket.username = null;
        socket.authenticated = false;

        socket.reply = function (status, message, callback) {
            if (message === undefined) {
                message = messages[status] || 'No infomation';
            }
            if (this.writable) {
                logger.log('info', status.toString() + ' ' + message);
                this.write(status.toString() + ' ' + message + '\r\n', callback);
            }
        };

        socket.addListener('data', function (data) {
            var parts = data.trim().split(" "),
                command = parts[0].trim().toUpperCase(),
                args = parts.slice(1, parts.length),
                callable = commands[command];
            logger.log('info', data.trim());
            if (!callable) {
                socket.reply(500);
            } else {
                callable.apply(socket, args);
            }
        });

        socket.addListener('end', function () {
            server.emit('client:disconnected', socket);
        });

        socket.addListener('error', function (err) {
            server.emit('client:error', err, socket);
        });

        socket.reply(220);
    });

    return server;
}

util.inherits(createServer, process.EventEmitter);

exports.createServer = createServer;