/*jslint node: true, nomen: true*/
'use strict';

var logger = require('./logger'),
    messages = require('./messages');

module.exports = {
    // Common commands
    'ABOR': function () { // Aborts a file transfer currently in progress
        this.reply(202);
    }, // Not implemented
    'CWD': function () {}, // :)
    'DELE': function () {}, // :)
    'LIST': function () {}, // :)
    'MDTM': function () { // Returns the last-modified time of given file
        this.reply(202);
    }, // Not implemented
    'MKD': function () {
        this.reply(202);
    }, // Not implemented
    'NLST': function () {
        this.reply(202);
    }, // Not implemented
    'PASS': function () {}, // :)
    'PASV': function () {
        var socket = this;
    }, // :)
    'PORT': function () {}, // :)
    'PWD': function () {
        this.reply(257, '"/"');
    }, // :)
    'QUIT': function () {
        this.reply(221);
        this.end();
    },
    'RETR': function () {}, // :)
    'RMD': function () {
        this.reply(202);
    }, // Not implemented
    'RNFR': function () {
        this.reply(202);
    }, // Not implemented
    'RNTO': function () {
        this.reply(202);
    }, // Not implemented
    'SITE': function () {
        this.reply(202);
    }, // Not implemented
    'SIZE': function () {
        this.reply(202);
    }, // Not implemented
    'STOR': function () {}, // :)
    'TYPE': function () {}, // :)
    'USER': function (args) {
        var socket = this;
        this.emit('command:user', args, function (password) {
            if (password === undefined || password === false) {
                socket.reply(230); // User logged in, Password no needed
            } else {
                socket.reply(331); // Username okay, Password needed
            }
        }, function () {
            socket.reply(530); // Not logged in
        });
    },
    // Less common commands
    'ACCT': function () {
        this.reply(202);
    }, // Not implemented
    'APPE': function () {
        this.reply(202);
    }, // Not implemented
    'CDUP': function () {
        this.reply(202);
    }, // Not implemented
    'HELP': function () {
        this.reply(202);
    }, // Not implemented
    'MODE': function () {
        this.reply(202);
    }, // Not implemented
    'NOOP': function () {
        this.reply(202);
    }, // Not implemented
    'REIN': function () {
        this.reply(202);
    }, // Not implemented
    'STAT': function () {
        this.reply(202);
    }, // Not implemented
    'STOU': function () {
        this.reply(202);
    }, // Not implemented
    'STRU': function () {
        this.reply(202);
    }, // Not implemented
    'SYST': function () {
        this.reply(202);
    } // Not implemented
};