/*jslint node: true, nomen: true*/
'use strict';

var colors = require('colors');

module.exports = {
    debugLevel: 'info',
    log: function (level, message) {
        var levels = ['info', 'warn', 'error'];
        if (levels.indexOf(level) >= levels.indexOf(this.debugLevel)) {
            switch (level) {
            case 'info':
                console.log((level + ': ').green + message);
                break;
            case 'warn':
                console.log((level + ': ').yellow + message);
                break;
            case 'error':
                console.log((level + ': ').red + message);
                break;
            }
        }
    }
};