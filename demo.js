/*jslint node: true, nomen: true*/
'use strict';

var ftpd = require('node-ftpd'),
    server = ftpd.createServer();

server.on('client:connected', function (socket) {
    socket.on('command:user', function (args, success, failure) {
        if (args) {
            success();
        } else {
            failure();
        }
    });
});

server.listen(7000, function () {
    console.log('listening on port ' + server.address().port);
});